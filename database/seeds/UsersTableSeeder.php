<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => "Admin",
                'email' => 'admin@mthelmetsbd.com',
                'email_verified_at' => now(),
                'password' => Hash::make("admin123"), // password
                'remember_token' => Str::random(10),
            ]
        ]);
    }
}
